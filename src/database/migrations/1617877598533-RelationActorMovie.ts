import { MigrationInterface, QueryRunner } from "typeorm";

export default class RelationActorMovie1617877598533 implements MigrationInterface {
  name = 'RelationActorMovie1617877598533'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE "actors__movies" ("id" SERIAL NOT NULL, CONSTRAINT "PK_ba3321b1021bfa1accf1b654dab" PRIMARY KEY ("id"))`);
    await queryRunner.query(`CREATE TABLE "movies_actors_actors" ("moviesId" integer NOT NULL, "actorsId" integer NOT NULL, CONSTRAINT "PK_81ffbd4dab2aab2970909e04035" PRIMARY KEY ("moviesId", "actorsId"))`);
    await queryRunner.query(`CREATE INDEX "IDX_638b1d6f6929495fa5b87206da" ON "movies_actors_actors" ("moviesId") `);
    await queryRunner.query(`CREATE INDEX "IDX_6f9bbef3136f7efc40a5a55886" ON "movies_actors_actors" ("actorsId") `);
    await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3"`);
    await queryRunner.query(`ALTER TABLE "movies_actors_actors" ADD CONSTRAINT "FK_638b1d6f6929495fa5b87206daf" FOREIGN KEY ("moviesId") REFERENCES "movies"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    await queryRunner.query(`ALTER TABLE "movies_actors_actors" ADD CONSTRAINT "FK_6f9bbef3136f7efc40a5a55886c" FOREIGN KEY ("actorsId") REFERENCES "actors"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "movies_actors_actors" DROP CONSTRAINT "FK_6f9bbef3136f7efc40a5a55886c"`);
    await queryRunner.query(`ALTER TABLE "movies_actors_actors" DROP CONSTRAINT "FK_638b1d6f6929495fa5b87206daf"`);
    await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email")`);
    await queryRunner.query(`DROP INDEX "IDX_6f9bbef3136f7efc40a5a55886"`);
    await queryRunner.query(`DROP INDEX "IDX_638b1d6f6929495fa5b87206da"`);
    await queryRunner.query(`DROP TABLE "movies_actors_actors"`);
    await queryRunner.query(`DROP TABLE "actors__movies"`);
  }

}
