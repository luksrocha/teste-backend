import Actors from '../entities/Actors';
import { Repository, EntityRepository } from 'typeorm';

@EntityRepository(Actors)
class ActorsRepository extends Repository<Actors>{

  public async findById(id: string): Promise<Actors | undefined> {
    const actor = await this.findOne(id)

    return actor;
  }

}

export default ActorsRepository;