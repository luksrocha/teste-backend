import { Router } from "express";
import MovieController from "../controllers/MoviesActorsController";
import AuthMiddleware from '../../../middlewares/auth';

const movieActorRouter = Router();
const movieController = new MovieController();

movieActorRouter.use(AuthMiddleware);
movieActorRouter.post('/', movieController.create);

export default movieActorRouter;
