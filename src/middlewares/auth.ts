import { Response, Request, NextFunction } from 'express';
import { promisify } from 'util'
import { verify } from 'jsonwebtoken';
import authConfig from '../utils/auth';

interface IDecoded {
  id: number,
  type: string
}

export default async (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    return res.status(401).json({ error: 'Token does not exists!' });
  }

  const [, token] = authHeader.split(' ');

  try {

    const decoded: IDecoded = verify(token, authConfig.secret);

    req.user = { user_id: decoded.id, type: decoded.type }

    return next();

  } catch (error) {
    return res.status(401).json({ error: 'Invalid token!' });
  }

  return next();
}