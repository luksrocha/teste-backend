import { Request, Response } from 'express';
import { getCustomRepository } from 'typeorm';
import MoviesRepository from '../../../repositories/MovieRepository';

class MovieController {
  public async index(req: Request, res: Response) {
    const moviesRepository = getCustomRepository(MoviesRepository);
    const { title, gender, director } = req.query;
    const { type } = req.user;

    if (type != 'admin') {
      return res.status(401).json({ message: 'Only admin can access that route' });
    }

    const where: any = {};

    if (title) {
      where.title = title;
    }

    if (gender) {
      where.gender = gender;
    }

    if (director) {
      where.director = director;
    }

    const movies = await moviesRepository.find({
      where,
      relations: ['actors'],
    });

    return res.json(movies);
  }

  public async show(req: Request, res: Response) {
    const moviesRepository = getCustomRepository(MoviesRepository);
    const { type } = req.user;
    const { id } = req.params;

    if (type != 'admin') {
      return res.status(401).json({ message: 'Only admin can access that route' });
    }

    const movies = await moviesRepository.findById(id);

    if (!movies) {
      return res.status(401).json({ message: 'Movie not found' });
    }

    return res.status(200).json(movies);
  }

  public async create(req: Request, res: Response) {
    const moviesRepository = getCustomRepository(MoviesRepository);
    const {
      title, description, gender, director,
    } = req.body;
    const { type } = req.user;

    if (type != 'admin') {
      return res.status(401).json({ message: 'Only admin can access that route' });
    }

    const movieExists = await moviesRepository.findByTitle(title);

    if (movieExists) {
      return res.status(401).json({ message: 'Movie already exists' });
    }

    try {
      const movie = moviesRepository.create({
        title,
        description,
        gender,
        director,
      });

      moviesRepository.save(movie);

      return res.status(200).json(movie);
    } catch (error) {
      return res.status(401).json(error);
    }
  }

  public async delete(req: Request, res: Response) {
    const moviesRepository = getCustomRepository(MoviesRepository);
    const { id } = req.params;
    const { type } = req.user;

    if (type != 'admin') {
      return res.status(401).json({ message: 'Only admin can access that route' });
    }

    const movie = await moviesRepository.findById(id);

    if (!movie) {
      return res.status(401).json({ message: 'Movie not found.' });
    }

    try {
      await moviesRepository.softRemove(movie);
      return res.json({ message: 'Movie deleted' });
    } catch (error) {
      return res.json(error);
    }
  }
}

export default MovieController;
