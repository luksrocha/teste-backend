import { Router } from 'express';
import ActorController from '../controllers/ActorControllers';
import AuthMiddleware from '../../../middlewares/auth';

const actorRouter = Router();
const actorController = new ActorController();

actorRouter.use(AuthMiddleware);
actorRouter.post('/', actorController.create);

export default actorRouter;