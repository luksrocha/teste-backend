import Movies from '../entities/Movies';
import { Repository, EntityRepository } from 'typeorm';

@EntityRepository(Movies)
class MoviesRepository extends Repository<Movies>{

  public async findByTitle(title: string): Promise<Movies | undefined> {
    const user = await this.findOne({
      where: {
        title
      }
    })

    return user;
  }

  public async findById(id: string): Promise<Movies | undefined> {
    const user = await this.findOne(id)

    return user;
  }
}

export default MoviesRepository;