import { Entity, JoinColumn, ManyToMany, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import Movie from './Movies';
import Actor from './Actors';

@Entity('movies_actors_actors')
class MoviesActor {
  @PrimaryColumn()
  id: number

  @ManyToOne(type => Movie, movie => movie.id)
  @JoinColumn()
  movieId: Movie[]

  @ManyToOne(type => Actor, actor => actor.id)
  @JoinColumn()
  actorId: Actor[]

}

export default MoviesActor
