import { Request, Response } from "express";
import { getCustomRepository } from "typeorm";
import ActorsRepository from '../../../repositories/ActorRepository';

class ActorController {

  public async create(req: Request, res: Response) {
    const actorRepository = getCustomRepository(ActorsRepository);
    const { name } = req.body
    const { type } = req.user

    if (type != 'admin') {
      return res.status(401).json({ message: 'Only admin can access that route' })
    }

    try {
      const actor = actorRepository.create({
        name
      });

      actorRepository.save(actor);

      return res.status(200).json(actor)
    } catch (error) {
      return res.status(401).json(error)
    }

  }

}

export default ActorController;