import jwt from 'jsonwebtoken'
import { Request, Response } from 'express';
import { getCustomRepository } from 'typeorm';
import UserRepository from '../../../repositories/UserRepository';
import { compare } from 'bcryptjs';

import authConfig from '../../../utils/auth';

class SessionController {

  public async store(req: Request, res: Response) {
    const { email, password } = req.body

    console.log(email);

    const userRepository = getCustomRepository(UserRepository)

    const user = await userRepository.findByEmail(email);

    if (!user) {
      return res.status(401).json({ message: 'User not found' })
    }

    const checkPassword = await compare(password, user.password)

    if (!checkPassword) {
      return res.status(401).json({ message: 'Incorrect password' })
    }

    return res.json({
      user: {
        id: user.id,
        email: user.email,
        name: user.first_name + ' ' + user.last_name
      },
      token: jwt.sign({ id: user.id, type: user.type }, authConfig.secret, {
        expiresIn: authConfig.expiresIn
      })
    })

  }

}

export default SessionController;
