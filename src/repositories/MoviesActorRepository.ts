import MoviesActor from '../entities/MoviesActor';
import { Repository, EntityRepository } from 'typeorm';

@EntityRepository(MoviesActor)
class MoviesActorRepository extends Repository<MoviesActor>{ }

export default MoviesActorRepository;
