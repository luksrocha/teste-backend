import { Router } from "express";
import UserController from "../controllers/UserController";


import authMiddleware from '../../../middlewares/auth';

const userRouter = Router();
const userController = new UserController();

userRouter.post('/', userController.create);

userRouter.use(authMiddleware)
userRouter.get('/', userController.index);
userRouter.put('/:id', userController.update);
userRouter.delete('/:id', userController.delete);

export default userRouter;
