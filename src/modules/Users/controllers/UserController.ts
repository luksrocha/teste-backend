import { Response, Request } from 'express';
import UserRepository from '../../../repositories/UserRepository';
import { getCustomRepository } from 'typeorm';
import { hash, compare } from 'bcryptjs';
import User from 'src/entities/User';

interface IUpdate {
  first_name: string;
  email: string;
  old_password?: string;
  new_password?: string;
}

class UserController {

  public async index(req: Request, res: Response) {
    const userRepository = getCustomRepository(UserRepository)

    const user = await userRepository.find();

    return res.json(user)
  }

  public async create(req: Request, res: Response) {
    const { first_name, last_name, email, password, type } = req.body

    const userRepository = getCustomRepository(UserRepository)

    const hasUser = await userRepository.findByEmail(email);

    if (hasUser) {
      return res.json({ message: 'E-mail already taken' })
    }

    if (type == 'user' || type == 'admin') {

      const hashedPassword = await hash(password, 8);

      try {
        const user = userRepository.create({
          first_name,
          last_name,
          email,
          password: hashedPassword,
          type
        })

        userRepository.save(user);

        return res.status(200).json(user)

      } catch (error) {
        return res.status(401).json(error)
      }
    }

    return res.json({ message: "Only 'user' or 'admin' type is accepted." })
  }

  public async update(req: Request, res: Response) {
    const userRepository = getCustomRepository(UserRepository)
    const { id } = req.params
    const { user_id } = req.user
    const { first_name, email, old_password, new_password }: IUpdate = req.body

    const user = await userRepository.findById(id);

    if (!user) {
      return res.json({ message: 'User not found.' })
    }

    if (user.id !== user_id) {
      return res.json({ message: 'Can not change data from another person.' })
    }

    const emailAlreadyTaken = await userRepository.findByEmail(email)

    if (emailAlreadyTaken) {
      return res.json({ message: 'Email already taken' })
    }

    if (new_password && !old_password) {
      return res.json({ message: 'Old password is required' });
    }

    if (new_password && old_password) {
      const checkedPassword = await compare(old_password, user.password)

      if (!checkedPassword) {
        return res.json({ message: "Old password does not match" })
      }

      user.password = await hash(new_password, 8)
    }

    user.first_name = first_name;
    user.email = email;

    await userRepository.save(user);

    return res.status(200).json(user)

  }

  public async delete(req: Request, res: Response) {
    const userRepository = getCustomRepository(UserRepository);
    const { id } = req.params
    const { type, user_id } = req.user

    if (type != 'admin') {
      return res.status(401).json({ message: 'Only admin can delete' })
    }

    const user = await userRepository.findById(id)

    if (!user) {
      return res.json({ message: 'User not found.' })
    }

    if (user.type == 'admin' || user_id == user.id) {
      return res.json({ message: "Can not delete users with type 'admin'" })
    }

    try {
      await userRepository.softRemove(user);
      return res.json({ message: 'User deleted' })

    } catch (error) {
      return res.json(error)
    }


  }

}

export default UserController;