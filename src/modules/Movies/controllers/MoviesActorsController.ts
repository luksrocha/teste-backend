import { Request, Response } from 'express';
import { getCustomRepository } from 'typeorm';
import MoviesActorsRepository from '../../../repositories/MoviesActorRepository';

class MoviesActorsController {

  public async create(req: Request, res: Response) {
    const moviesActorRepository = getCustomRepository(MoviesActorsRepository)
    const { type } = req.user
    // const { id } = req.params
    const { actorId, movieId } = req.body

    if (type != 'admin') {
      return res.status(401).json({ message: 'Only admin can access that route' });
    }

    const savedMovieActor = await moviesActorRepository.create({
      actorId,
      movieId
    })

    await moviesActorRepository.save(savedMovieActor)

    return res.json(savedMovieActor)

  }

};

export default MoviesActorsController
