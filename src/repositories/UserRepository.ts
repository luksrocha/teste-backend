import User from '../entities/User';
import { Repository, EntityRepository } from 'typeorm';

@EntityRepository(User)
class UserRepository extends Repository<User>{

  public async findByEmail(email: string): Promise<User | undefined> {
    const user = await this.findOne({
      where: {
        email
      }
    });

    return user
  }

  public async findById(id: string): Promise<User | undefined> {
    const user = await this.findOne(id)

    return user;
  }
}

export default UserRepository;