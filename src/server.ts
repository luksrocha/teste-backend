import 'reflect-metadata';
import express from 'express';
import 'express-async-errors';
import './database';
import routes from './routes';
import cors from 'cors';

const app = express();
app.use(express.json());
app.use(cors());
app.use(routes);

app.listen(3333, () => {
  console.log('Server running on port 3333!');
});
