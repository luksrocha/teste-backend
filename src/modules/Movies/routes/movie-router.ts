import { Router } from "express";
import MovieController from "../controllers/MovieController";
import AuthMiddleware from '../../../middlewares/auth';

const movieRouter = Router();
const movieController = new MovieController();

movieRouter.use(AuthMiddleware);
movieRouter.post('/', movieController.create);
movieRouter.get('/', movieController.index);
movieRouter.get('/:id', movieController.show);
movieRouter.delete('/:id', movieController.delete);

export default movieRouter;