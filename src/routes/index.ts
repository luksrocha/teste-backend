import { Router } from 'express';
import movieActorRouter from '../modules/Movies/routes/movies-actor-router';
import actorRouter from '../modules/Actors/routes/actor-router';
import movieRouter from '../modules/Movies/routes/movie-router';
import sessionRouter from '../modules/Users/routes/session-routes';
import userRouter from '../modules/Users/routes/user-routes';

const routes = Router();

routes.use('/users', userRouter);
routes.use('/login', sessionRouter);
routes.use('/movie', movieRouter);
routes.use('/actor', actorRouter);
routes.use('/movie-actor', movieActorRouter)

export default routes;
